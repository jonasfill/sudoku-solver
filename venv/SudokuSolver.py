
def solve(board):
    # if sudoku is solved, return True
    next_field = empty_field(board)
    if not next_field:
        return True

    # try every number for current field
    for i in range(1, 10):
        if valid(board, i, next_field):
            # set new board entry
            board[next_field[0]][next_field[1]] = i
            if solve(board):
                return True
            # reset board entry if not successful
            board[next_field[0]][next_field[1]] = 0
    # if no number can be set, the current run wasn't successful
    return False


def empty_field(board):
    for i in range(9):
        for j in range(9):
            # return position of empty field
            if board[i][j] == 0:
                return i, j
    # return None if no empty field was found
    return None


def valid(board, num, pos):
    r, c = pos
    # check if entry violates row invariant
    for i in range(9):
        if i != c and board[r][i] == num:
            return False

    # check if entry violates column invariant
    for i in range(9):
        if i != r and board[i][c] == num:
            return False

    # check if entry violates block invariant
    upper_border_hor = c
    # determine upper horizontal border for current block. Borders are numbers dividable by 3
    while (upper_border_hor + 1) % 3 != 0:
        upper_border_hor += 1
    upper_border_hor += 1
    lower_border_hor = upper_border_hor - 3

    upper_border_ver = r
    # determine upper vertical border for current block. Borders are numbers dividable by 3
    while (upper_border_ver + 1) % 3 != 0:
        upper_border_ver += 1
    upper_border_ver += 1
    lower_border_ver = upper_border_ver - 3

    # search trough block
    for i in range(lower_border_ver, upper_border_ver):
        for j in range(lower_border_hor, upper_border_hor):
            if (i,j) != (r, c) and board[i][j] == num:
                return False
    return True


def print_board(board):
    for i in range(9):
        row = ""
        for j in range(9):
            if j == 9-1:
                row += str(board[i][j])
            elif (j+1) % 3 == 0:
                row += str(board[i][j]) + "\t|\t"
            else:
                row += str(board[i][j]) + "\t"
        print(row)
        if i == 9-1:
            continue
        elif (i+1) % 3 == 0:
            print
            dashes = ""
            for k in range(9):
                if k == 9-1:
                    dashes += "-"
                elif (k+1) % 3 == 0:
                    dashes += "-\t \t"
                else:
                    dashes += "-\t"
            print(dashes)
            print
        else:
            print

board = [
    [2,0,9,6,0,0,1,0,3],
    [0,0,8,4,0,9,0,0,7],
    [0,0,4,0,0,0,0,0,8],
    [5,0,0,0,9,0,6,0,0],
    [0,0,0,7,3,2,0,0,0],
    [3,0,0,0,0,0,4,0,0],
    [0,7,0,0,2,0,8,9,0],
    [0,9,0,0,0,0,3,1,0],
    [0,0,0,0,6,5,0,0,0],
]

"""
print_board(board)
print()
print()
print(solve(board))
print_board(board)
"""

