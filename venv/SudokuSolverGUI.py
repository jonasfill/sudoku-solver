import wx
import wx.grid
import SudokuSolver

class GridFrame(wx.Frame):
    def __init__(self, parent, title):
        wx.Frame.__init__(self, parent, title=title, size=wx.Size(360,382))

        # Create status bar
        self.CreateStatusBar()

        # Create menu bar

        # Setting up the menu.
        filemenu = wx.Menu()
        new_game = wx.MenuItem(filemenu, wx.ID_NEW, text="New game", helpString=" Start new game")
        filemenu.Append(new_game)

        # wx.ID_ABOUT and wx.ID_EXIT are standard IDs provided by wxWidgets.
        menu_about = filemenu.Append(wx.ID_ABOUT, "&About", " Information about this program")
        # filemenu.AppendSeparator()
        menu_exit = filemenu.Append(wx.ID_EXIT, "E&xit", " Quit the program")

        self.Bind(wx.EVT_MENU, self.OnAbout, menu_about)
        self.Bind(wx.EVT_MENU, self.OnNew, new_game)

        # Creating the file menubar.
        menuBar = wx.MenuBar()
        menuBar.Append(filemenu, "&File")  # Adding the "filemenu" to the MenuBar
        self.SetMenuBar(menuBar)  # Adding the MenuBar to the Frame content.



        # Create size
        self.sizer = wx.BoxSizer(wx.VERTICAL)

        # Create a wxGrid object
        self.grid = wx.grid.Grid(self, -1)

        # Hide labels
        self.grid.HideColLabels()
        self.grid.HideRowLabels()

        # Then we call CreateGrid to set the dimensions of the grid
        # (100 rows and 10 columns in this example)
        self.grid.CreateGrid(9, 9)

        """
        self.grid.SetColFormatNumber(0)
        self.grid.SetColFormatNumber(1)
        self.grid.SetColFormatNumber(2)
        self.grid.SetColFormatNumber(3)
        self.grid.SetColFormatNumber(4)
        self.grid.SetColFormatNumber(5)
        self.grid.SetColFormatNumber(6)
        self.grid.SetColFormatNumber(7)
        self.grid.SetColFormatNumber(8)
        """
        # Set size of all cells
        row_sizes = wx.grid.GridSizesInfo(40, [])
        col_sizes = wx.grid.GridSizesInfo(40, [])
        self.grid.SetRowSizes(row_sizes)
        self.grid.SetColSizes(col_sizes)
        self.grid.SetCellHighlightPenWidth(2)

        # Set font and alignment
        self.grid.SetDefaultCellAlignment(wx.ALIGN_CENTRE, wx.ALIGN_CENTRE)
        font = wx.Font(20, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD)
        self.grid.SetDefaultCellFont(font)

        # And set grid cell contents as strings
        # grid.SetCellValue(0, 0, 'wxGrid is good')

        # We can specify that some cells are read.only
        # grid.SetCellValue(0, 3, 'This is read.only')
        # grid.SetReadOnly(0, 3)

        # Colours can be specified for grid cell contents
        # grid.SetCellValue(3, 3, 'green on grey')
        # grid.SetCellTextColour(3, 3, wx.GREEN)
        # grid.SetCellBackgroundColour(3, 3, wx.LIGHT_GREY)

        # We can specify the some cells will store numeric
        # values rather than strings. Here we set grid column 5
        # to hold floating point values displayed with width of 6
        # and precision of 2
        # grid.SetColFormatFloat(5, 6, 2)
        # grid.SetCellValue(0, 6, '3.1415')

        # Button to solve sudoku
        self.solve_button = wx.Button(self, label="Solution")

        # Bind button to event
        self.Bind(wx.EVT_BUTTON, self.EvtSolveButton, self.solve_button)

        # Add components to sizer
        self.sizer.Add(self.grid, 10, wx.EXPAND)
        self.sizer.Add(self.solve_button, 1, wx.EXPAND | wx.LEFT | wx.RIGHT, 30)

        # Set sizer
        self.SetSizer(self.sizer)
        self.SetAutoLayout(True)
        self.sizer.Fit(self)

        self.Show()

    def fill_board(self, board):
        for i in range(9):
            for j in range(9):
                if board[i][j] != 0:
                    # Set value and field to read only
                    self.grid.SetCellValue(i, j, str(board[i][j]))
                    self.grid.SetReadOnly(i, j)

    def empty_board(self):
        for i in range(9):
            for j in range(9):
                self.grid.SetCellValue(i, j, "")
                # set board to read-write
                self.grid.SetReadOnly(i, j, isReadOnly=False)

    def read_board_int(self):
        board = [[0 for _ in range(9)] for _ in range(9)]
        for i in range(9):
            for j in range(9):
                value = self.grid.GetCellValue(i, j)
                if value != "":
                    # Set value and field to read only
                    board[i][j] = int(value)
                else:
                    board[i][j] = 0
        return board

    def solve(self):
        board = self.read_board_int()
        if SudokuSolver.solve(board):
            self.fill_board(board)
        else:
            dialog_no_solution = wx.MessageDialog(self, "The given Sudoku has no solution!", "Error", style=wx.OK)
            dialog_no_solution.ShowModal()
            dialog_no_solution.Destroy()

    def EvtSolveButton(self, event):
        self.solve()

    # Event when the aboutButton is hit
    def OnAbout(self, event):
        # A message dialog box with an OK button. wx.OK is a standard ID in wxWidgets.
        dlg = wx.MessageDialog(self, "A small sudoku solver by Jonas Fill", "About SudokuSolver", wx.OK)
        dlg.ShowModal()  # Show it
        dlg.Destroy()  # finally destroy it when finished.

    # Event when the new game button is hit
    def OnNew(self, event):
        self.empty_board()


if __name__ == '__main__':
    """
    board = [
        [2, 0, 9, 6, 0, 0, 1, 0, 3],
        [0, 0, 8, 4, 0, 9, 0, 0, 7],
        [0, 0, 4, 0, 0, 0, 0, 0, 8],
        [5, 0, 0, 0, 9, 0, 6, 0, 0],
        [0, 0, 0, 7, 3, 2, 0, 0, 0],
        [3, 0, 0, 0, 0, 0, 4, 0, 0],
        [0, 7, 0, 0, 2, 0, 8, 9, 0],
        [0, 9, 0, 0, 0, 0, 3, 1, 0],
        [0, 0, 0, 0, 6, 5, 0, 0, 0],
    ]
    """
    app = wx.App(0)
    frame = GridFrame(None, "SudokuSolver")
    # frame.fill_board(board)
    app.MainLoop()